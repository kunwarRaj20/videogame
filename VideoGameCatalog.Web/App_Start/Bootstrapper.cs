﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using VideoGameCatalog.Web.Infrastructure.Mappings;

namespace VideoGameCatalog.Web.App_Start
{
    public class Bootstrapper
    {
        public static void Run()
        {
            AutofacWebapiConfig.Initlaize(GlobalConfiguration.Configuration);

            AutoMapperConfiguration.Configure();
        }
    }
}