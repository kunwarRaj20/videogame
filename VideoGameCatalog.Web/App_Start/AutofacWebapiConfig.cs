﻿using Autofac;
using Autofac.Integration.WebApi;
using System.Data.Entity;
using System.Reflection;
using System.Web.Http;
using VideoGameCatalog.EntityFramework;
using VideoGameCatalog.EntityFramework.Infrastructure;
using VideoGameCatalog.EntityFramework.Repositories;

namespace VideoGameCatalog.Web.App_Start
{
    public class AutofacWebapiConfig
    {
        public static IContainer Container;

        public static void Initlaize(HttpConfiguration config)
        {
            Initlaize(config, RegisterServices(new ContainerBuilder()));
        }

        public static void Initlaize(HttpConfiguration config, IContainer container)
        {
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static IContainer RegisterServices(ContainerBuilder builder)
        {
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<VideoGameCatalogDbContext>()
                .As<DbContext>()
                .InstancePerRequest();

            builder.RegisterType<UnitOfWork>()
                .As<IUnitOfWork>()
                .InstancePerRequest();

            builder.RegisterType<DbFactory>()
                .As<IDbFactory>()
                .InstancePerRequest();

            builder.RegisterGeneric(typeof(EntityBaseRepository<>))
                .As(typeof(IEntityBaseRepository<>))
                .InstancePerRequest();

            Container = builder.Build();

            return Container;
        }
    }
}