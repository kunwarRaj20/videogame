﻿using System.Web.Optimization;

namespace VideoGameCatalog.Web.App_Start
{
    public static class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/Vendors/modernizr.js"));

            bundles.Add(new ScriptBundle("~/bundles/vendors").Include(
                "~/Scripts/Vendors/jquery/jquery-3.3.1.js",
                "~/Scripts/Vendors/umd/popper.js",
                "~/Scripts/Vendors/bootstrap/bootstrap.js",
                "~/Scripts/Vendors/toastr/toastr.js",
                "~/Scripts/Vendors/angular/angular.js",
                "~/Scripts/Vendors/AngularUI/angular-ui-router.js",
                "~/Scripts/Vendors/AngularUI/ui-utils.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/spa")
                .IncludeDirectory("~/Scripts/spa", "*.js", true)
                );

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/content/css/site.css",
                "~/content/css/bootstrap/bootstrap.css",
                "~/content/css/font-awesome.css",
                "~/content/css/toastr/toastr.css"
                ));
        }
    }
}