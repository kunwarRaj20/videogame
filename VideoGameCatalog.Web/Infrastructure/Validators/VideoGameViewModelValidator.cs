﻿using FluentValidation;
using VideoGameCatalog.Web.Models;

namespace VideoGameCatalog.Web.Infrastructure.Validators
{
    public class VideoGameViewModelValidator : AbstractValidator<VideoGameViewModel>
    {
        public VideoGameViewModelValidator()
        {
            RuleFor(videoGame => videoGame.Title).NotEmpty()
                .WithMessage("Title can't be empty");

            RuleFor(videoGame => videoGame.Description).NotEmpty()
                .WithMessage("Description can't be empty");
        }
    }
}