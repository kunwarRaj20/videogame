﻿using System;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VideoGameCatalog.Core;
using VideoGameCatalog.EntityFramework.Infrastructure;
using VideoGameCatalog.EntityFramework.Repositories;

namespace VideoGameCatalog.Web.Infrastructure.Core
{
    public class ApiControllerBase: ApiController
    {
        protected readonly IEntityBaseRepository<ErrorLog> _errorRepository;
        protected readonly IUnitOfWork _unitOfWork;

        public ApiControllerBase(IEntityBaseRepository<ErrorLog> errorRepository, IUnitOfWork unitOfWork)
        {
            _errorRepository = errorRepository;
            _unitOfWork = unitOfWork;
        }

        protected HttpResponseMessage CreateHttpResponse(HttpRequestMessage request, Func<HttpResponseMessage> function)
        {
            HttpResponseMessage response = null;
            try
            {
                response = function.Invoke();
            }
            catch (DbUpdateException ex)
            {
                LogError(ex);
                response = request.CreateResponse(HttpStatusCode.BadRequest, ex.InnerException.Message);
            }
            catch (Exception ex)
            {
                LogError(ex);
                response = request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            return response;
        }

        private void LogError(Exception ex)
        {
            try
            {
                ErrorLog _errorLog = new ErrorLog()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                    CreationTime = DateTime.UtcNow
                };

                _errorRepository.Add(_errorLog);
                _unitOfWork.Commit();
            }
            catch 
            {           
                
            }
        }
    }
}