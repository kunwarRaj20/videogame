﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VideoGameCatalog.Core;
using VideoGameCatalog.Web.Models;

namespace VideoGameCatalog.Web.Infrastructure.Mappings
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<VideoGame, VideoGameViewModel>()
            .ForMember(q => q.Category, map => map.MapFrom(m => m.Category.Title))
            .ForMember(q => q.CategoryId, map => map.MapFrom(m => m.Category.Id))
            .ForMember(q => q.ImageURL, map => map.MapFrom(m => string.IsNullOrEmpty(m.ImageURL) == true ? "unknown.jpg" : m.ImageURL));
        }

    }
}