﻿using VideoGameCatalog.Core;
using VideoGameCatalog.Web.Models;

namespace VideoGameCatalog.Web.Infrastructure.Extensions
{
    public static class EntitiesExtensions
    {
        public static void UpdateVideoGame(this VideoGame videoGame, VideoGameViewModel videoGameVm)
        {
            videoGame.Title = videoGameVm.Title;
            videoGame.Description = videoGameVm.Description;
            videoGame.Creator = videoGameVm.Creator;
            videoGame.Rating = videoGameVm.Rating;
            videoGame.ReleaseDate = videoGameVm.ReleaseDate;
        }
    }
}