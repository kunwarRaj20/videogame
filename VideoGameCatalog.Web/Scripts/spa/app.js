﻿angular.module('videoGameCatalog', ['ui.router'])
    .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', config]);

function config($stateProvider, $urlRouterProvider, $locationProvider) {

    $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix = '!';

    $urlRouterProvider.otherwise("/home");

    $stateProvider.state('home', {
        url: '/home',
        templateUrl: 'Scripts/spa/home/index.html',
        controller:'HomeController'
    }
    )
        .state('edit', {
            url: '/edit',
            templateUrl: 'scripts/spa/edit/index.html',
            controller: function ($scope, $stateParams) {
                $scope.id = $stateParams.id;
            },
            params: {
                id: null
            }
        });
}