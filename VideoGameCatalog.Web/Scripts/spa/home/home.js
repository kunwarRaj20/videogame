﻿(function (app) {
    'use strict';

    app.controller('HomeController', ['apiService', '$state', 'notificationService', homeController]);

    function homeController(apiService, $state, notificationService) {
        var vm = this;

        loadData();
        vm.editItem = editItem;

        function loadData() {
            apiService.get('/api/videogame/', null,
                function (result) {
                    vm.videoGames = result.data;
                },
                function (response) {
                    notificationService.displayError(response.data);
                });
        }

        function editItem(id) {
            localStorage.setItem('videoGameId', id);
            $state.go('edit', {
                id: id
            });
        }
    }
})(angular.module('videoGameCatalog'));