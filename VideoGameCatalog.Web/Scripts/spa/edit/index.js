﻿(function (app) {
    'use strict';

    app.controller('EditController', ['$stateParams', 'apiService', '$state', 'notificationService', editController]);

    function editController($stateParams, apiService, $state, notificationService) {

        var vm = this;

        vm.saving = true;
        vm.id = ($stateParams.id === null) ? localStorage.getItem('videoGameId') : $stateParams.id;

        vm.ratings = [
            { 'id': 1, 'label': '1' },
            { 'id': 2, 'label': '2' },
            { 'id': 3, 'label': '3' },
            { 'id': 4, 'label': '4' },
            { 'id': 5, 'label': '5' }
        ];

        getData(vm.id);
        vm.saveData = saveData;
        vm.cancel = cancel;

        function getData(id) {
            var config = {
                params: {
                    id: id
                }
            };

            apiService.get('/api/videogame/edit/', config,
                function (result) {
                    vm.videoGame = result.data;
                },
                function (response) {
                    notificationService.displayError(response.data);
                });
        }


        function saveData(isValid) {
            if (isValid) {
                vm.saving = true;

                apiService.post('/api/videogame/update', vm.videoGame,
                    function () {
                        notificationService.displaySuccess("Saved Successfully");
                    },
                    function (response) {
                        notificationService.displayError("Please fill all the fields");
                    });

                vm.saving = false;
            }
        }

        function cancel() {
            $state.go('home', {});
        }
    }

})(angular.module('videoGameCatalog'));