﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using VideoGameCatalog.Web.Models;

namespace VideoGameCatalog.Web.Controllers
{
    public interface IVideoGameController
    {
        HttpResponseMessage GetAll(HttpRequestMessage request);

        HttpResponseMessage Update(HttpRequestMessage request, VideoGameViewModel videoGame);

        HttpResponseMessage GetVideoGame(HttpRequestMessage request, Guid id);
    }
}
