﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using VideoGameCatalog.Core;
using VideoGameCatalog.EntityFramework.Infrastructure;
using VideoGameCatalog.EntityFramework.Repositories;
using VideoGameCatalog.Web.Infrastructure.Core;
using VideoGameCatalog.Web.Infrastructure.Extensions;
using VideoGameCatalog.Web.Models;

namespace VideoGameCatalog.Web.Controllers
{
    [RoutePrefix("api/videogame")]
    public class VideoGameController : ApiControllerBase,IVideoGameController
    {
        private readonly IEntityBaseRepository<VideoGame> _videoGameRepository;
            

        public VideoGameController(IEntityBaseRepository<VideoGame> videoGameRepository,
            IEntityBaseRepository<ErrorLog> errorRepository,
            IUnitOfWork unitOfWork) : base(errorRepository, unitOfWork)
        {
            _videoGameRepository = videoGameRepository;
        }
        
        //[HttpGet]
        [Route("")]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var videoGames = _videoGameRepository.GetAll().OrderByDescending(q => q.ReleaseDate).ToList();

                IEnumerable<VideoGameViewModel> vgModel = Mapper.Map<IEnumerable<VideoGame>, IEnumerable<VideoGameViewModel>>(videoGames);

                response = request.CreateResponse(HttpStatusCode.OK, vgModel);
                return response;
            });
        }

        [HttpPost]
        [Route("update")]
        public HttpResponseMessage Update(HttpRequestMessage request, VideoGameViewModel videoGame)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                if (!ModelState.IsValid)
                {
                    response = request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var videoGameDb = _videoGameRepository.GetSingle(videoGame.Id);
                    if (videoGameDb == null)
                        response = request.CreateErrorResponse(HttpStatusCode.NotFound, "Invalid movie.");
                    else
                    {
                        videoGameDb.UpdateVideoGame(videoGame);
                        videoGame.ImageURL = videoGameDb.ImageURL;
                        _videoGameRepository.Edit(videoGameDb);

                        _unitOfWork.Commit();
                        response = request.CreateResponse<VideoGameViewModel>(HttpStatusCode.OK, videoGame);
                    }
                }

                return response;
            });
        }

        [Route("edit/{id:guid}")]
        public HttpResponseMessage GetVideoGame(HttpRequestMessage request, Guid id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var videoGame = _videoGameRepository.GetSingle(id);

                VideoGameViewModel videoGameVM = Mapper.Map<VideoGame, VideoGameViewModel>(videoGame);

                response = request.CreateResponse<VideoGameViewModel>(HttpStatusCode.OK, videoGameVM);

                return response;
            });
        }
    }
}