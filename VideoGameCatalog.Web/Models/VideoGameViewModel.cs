﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using VideoGameCatalog.Web.Infrastructure.Validators;

namespace VideoGameCatalog.Web.Models
{
    public class VideoGameViewModel: IValidatableObject
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageURL { get; set; }

        public string Category { get; set; }
        public Guid CategoryId { get; set; }
        public string Creator { get; set; }
        public DateTimeOffset ReleaseDate { get; set; }
        public byte Rating { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var validator = new VideoGameViewModelValidator();
            var result = validator.Validate(this);

            return result.Errors.Select(item => new ValidationResult(item.ErrorMessage, new[] { item.PropertyName }));
        }

    }
}