﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VideoGameCatalog.Core
{
    public class VideoGame :IEntityBase
    {
        public VideoGame()
        {
            Quantities = new List<Quantity>();
        }

        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string ImageURL { get; set; }
        
        public Guid CategoryId { get; set; }

        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }

        public string Creator { get; set; }

        public DateTimeOffset ReleaseDate {get;set;}

        public byte Rating { get; set; }

        public virtual ICollection<Quantity> Quantities { get; set; }


    }
}
