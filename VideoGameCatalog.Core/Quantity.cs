﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace VideoGameCatalog.Core
{
    public class Quantity: IEntityBase
    {
        public Guid Id { get; set; }

        public Guid VideoGameId { get; set; }

        [ForeignKey("VideoGameId")]
        public virtual VideoGame VideoGame { get; set; }

        public bool IsAvailable { get; set; }

    }
}
