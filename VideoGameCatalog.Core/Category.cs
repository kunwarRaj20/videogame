﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace VideoGameCatalog.Core
{
    public class Category : IEntityBase
    {
        public const int NameLength = 200;
        public const int DescriptionLength = 500;
        public Category()
        {
            VideoGames = new List<VideoGame>();
        }

        public Guid Id { get; set; }

        [StringLength(NameLength)]
        [MaxLength(NameLength)]
        public string Title { get; set; }

        [StringLength(DescriptionLength)]
        [MaxLength(DescriptionLength)]
        public string Description { get; set; }

        public virtual ICollection<VideoGame> VideoGames { get; set; }
    }
}
