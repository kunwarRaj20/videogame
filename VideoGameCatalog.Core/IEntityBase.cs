﻿using System;

namespace VideoGameCatalog.Core
{
    public interface IEntityBase
    {  
        Guid Id { get; set; }
    }
}
