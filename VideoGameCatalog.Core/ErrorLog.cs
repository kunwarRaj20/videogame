﻿using System;

namespace VideoGameCatalog.Core
{
    public class ErrorLog: IEntityBase
    {
        public Guid Id { get; set; }

        public string Message { get; set; }

        public string StackTrace { get; set; }

        public DateTimeOffset CreationTime { get; set; }
    }
}