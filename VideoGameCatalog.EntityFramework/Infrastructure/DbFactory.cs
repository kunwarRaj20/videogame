﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VideoGameCatalog.EntityFramework.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        VideoGameCatalogDbContext dbContext;

        public VideoGameCatalogDbContext Init()
        {
            return dbContext ?? (dbContext = new VideoGameCatalogDbContext());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}
