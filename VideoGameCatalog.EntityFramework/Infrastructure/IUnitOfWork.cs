﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VideoGameCatalog.EntityFramework.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}
