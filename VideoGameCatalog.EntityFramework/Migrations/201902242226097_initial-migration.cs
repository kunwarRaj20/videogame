namespace VideoGameCatalog.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialmigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Category",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Title = c.String(maxLength: 200),
                        Description = c.String(maxLength: 500),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.VideoGame",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Title = c.String(),
                        Description = c.String(),
                        ImageURL = c.String(),
                        CategoryId = c.Guid(nullable: false),
                        Creator = c.String(),
                        ReleaseDate = c.DateTimeOffset(nullable: false, precision: 7),
                        Rating = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Category", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.Quantity",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        VideoGameId = c.Guid(nullable: false),
                        IsAvailable = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.VideoGame", t => t.VideoGameId, cascadeDelete: true)
                .Index(t => t.VideoGameId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Quantity", "VideoGameId", "dbo.VideoGame");
            DropForeignKey("dbo.VideoGame", "CategoryId", "dbo.Category");
            DropIndex("dbo.Quantity", new[] { "VideoGameId" });
            DropIndex("dbo.VideoGame", new[] { "CategoryId" });
            DropTable("dbo.Quantity");
            DropTable("dbo.VideoGame");
            DropTable("dbo.Category");
        }
    }
}
