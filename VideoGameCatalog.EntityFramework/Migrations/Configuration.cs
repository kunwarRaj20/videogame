namespace VideoGameCatalog.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using VideoGameCatalog.Core;

    internal sealed class Configuration : DbMigrationsConfiguration<VideoGameCatalogDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(VideoGameCatalogDbContext context)
        {
            context.Categories.AddOrUpdate(GenerateCategories());
            context.SaveChanges();
            context.VideoGames.AddOrUpdate(GenerateVideoGames(context));
            context.SaveChanges();
            context.Quantities.AddOrUpdate(GenerateQuantities(context));
        }
        private VideoGame[] GenerateVideoGames(VideoGameCatalogDbContext context)
        {
            var videoGames = new VideoGame[] {
                new VideoGame()
                {
                    Id = Guid.Parse("B2EE6C13-218A-4D36-ACFE-AA4739580DC4"),
                    Title = "Angelo Skate Away",
                    Description = "Angelo, Lola, Victor and all their friends from the TV show �Angelo Rules� are here to make a ruckus! Skate, jump, perform acrobatic tricks, and glide through levels to unlock new backgrounds, characters, hats, figures and prove to your friends who�s the best skater in the neighborhood!",
                    ImageURL = "AngeloSkateAway.jpg",
                    CategoryId = context.Categories.FirstOrDefault(q => q.Title == "Kids").Id,
                    Creator = "Plug In Digital",
                    ReleaseDate = new DateTimeOffset(new DateTime(2018,4,29)),
                    Rating = 4
                },
                new VideoGame()
                {
                    Id = Guid.Parse("AF2DC108-19B0-4E0E-ABE1-50A14A35DFD1"),
                    Title = "Kirikou and the wild beasts",
                    Description = "Adapted from the famous movie by Michel Ocelot and Benedict Galup, Kirikou and the Wild Beasts puts you in the shoes of a hero who must face the dangers hanging over his people. Jump from platform to platform to protect the village from the wild animals of the savannah. But beware, Karaba the witch is never far away �",
                    ImageURL = "KirikouAndTheWildBeasts.jpg",
                    CategoryId = context.Categories.FirstOrDefault(q => q.Title == "Kids").Id,
                    Creator = "Emme Interactive",
                    ReleaseDate = new DateTimeOffset(new DateTime(2013,7,3)),
                    Rating = 3
                },
                new VideoGame()
                {
                    Id = Guid.Parse("B9B19510-AF71-4645-A938-054ECABCDF22"),
                    Title = "Momonga Pinball Adventure",
                    Description = "Cute and colorful as can be, this new pinball game will take you to a charming universe filled with adorable animals! Launch Momo, a flying squirrel, through the many levels of Momonga to defend its people agains the evil owls who have reduced Momo�s village to ashes.",
                    ImageURL = "MomongaPinballAdventure.jpg",
                    CategoryId = context.Categories.FirstOrDefault(q => q.Title == "Kids").Id,
                    Creator = "Plug In Digital",
                    ReleaseDate = new DateTimeOffset(new DateTime(2014,8,12)),
                    Rating = 2
                },
                new VideoGame()
                {
                    Id = Guid.Parse("01A6D194-E447-4A00-A654-6978FA3C03CA"),
                    Title = "Stickman Bike: Pro Ride",
                    Description = "Get ready to ride on different tracks each time more difficult. Make freestyle jumps, avoid obstacles and manage your speed to reach the finish line in the allotted time.",
                    ImageURL = "StickmanBikeProRide.jpg",
                    CategoryId = context.Categories.FirstOrDefault(q => q.Title == "Kids").Id,
                    Creator = "Playtouch",
                    ReleaseDate = new DateTimeOffset(new DateTime(2019,1,1)),
                    Rating = 3
                },
                new VideoGame()
                {
                    Id = Guid.Parse("09EEFA58-1137-4A16-BB43-FD884235EFA0"),
                    Title = "Chroma",
                    Description = "Welcome to Chroma, a simple and efficient brain teaser in which you are a colored square. Change your color to add the surrounding squares to you. When there is only one color left, you win.",
                    ImageURL = "Chroma.jpg",
                    CategoryId = context.Categories.FirstOrDefault(q => q.Title == "Kids").Id,
                    Creator = "Playtouch",
                    ReleaseDate = new DateTimeOffset(new DateTime(2016,6,19)),
                    Rating = 5
                },
                new VideoGame()
                {
                    Id = Guid.Parse("FC7E255E-FC66-4927-9E35-48FE15ED3222"),
                    Title = "Tennis World Tour",
                    Description = "A no-nonsense pro tennis simulator. Spin your way to greatness as one of the 30 top players in the world. Play on the same surfaces the pros do and win by using all the different types of shots. There�s even a career mode so you can see what it takes to rise through the ranks. A great game to play when the weather is bad and you�re in the mood to ace some balls.",
                    ImageURL = "TennisWorldTour.jpg",
                    CategoryId = context.Categories.FirstOrDefault(q => q.Title == "Sport").Id,
                    Creator = "Bigben Interactive",
                    ReleaseDate = new DateTimeOffset(new DateTime(2015,3,12)),
                    Rating = 4
                },
                new VideoGame()
                {
                    Id = Guid.Parse("B12F8F5E-02BB-4CC3-AE9C-4BAF329C0AEC"),
                    Title = "A Blind Legend",
                    Description = "You are Sir Edward Blake, a blind knight. Your wife has been abducted by tyrants and you must find her. You�ll have to listen carefully though, because there are no visuals in this game. Just like Sir Blake, you must navigate through space using only your sense of hearing and the help of your daughter. We really like the fact that this game helps raise awareness for the hearing impaired.",
                    ImageURL = "ABlingLenged.jpg",
                    CategoryId = context.Categories.FirstOrDefault(q => q.Title == "Adventure").Id,
                    Creator = "Plug In Digital",
                    ReleaseDate = new DateTimeOffset(new DateTime(2017,1,30)),
                    Rating = 2
                },
                new VideoGame()
                {
                    Id = Guid.Parse("1C7CC244-E96F-4725-89F7-B7A2C3C967D1"),
                    Title = "White Night",
                    Description = "After a car accident in a mysterious forest, you find shelter in an old and sinister mansion. But as you explore your new environment, strange events start to unfold around you. Will you have the courage to reveal the macabre past of the house? Investigate and discover what is hidden in the light, because the truth will come out of the shadows �",
                    ImageURL = "WhiteNight.jpg",
                    CategoryId = context.Categories.FirstOrDefault(q => q.Title == "Investigation").Id,
                    Creator = "Plug In Digital",
                    ReleaseDate = new DateTimeOffset(new DateTime(2018,9,15)),
                    Rating = 5
                },
                new VideoGame()
                {
                    Id = Guid.Parse("8E2EB0C1-F04F-4DE7-8339-52E1D329A95D"),
                    Title = "Garfield Kart",
                    Description = "The laziest cat in the world has finally gotten off the sofa. Join Garfield and friends in the funniest fast and fur-ious game yet. Take part in chaotic races where anything goes and be the first to cross the finish line, but watch out, you might get hit with a lasagna.",
                    ImageURL = "GarfieldKart.jpg",
                    CategoryId = context.Categories.FirstOrDefault(q => q.Title == "Speed").Id,
                    Creator = "Micro�ds",
                    ReleaseDate = new DateTimeOffset(new DateTime(2003,2,25)),
                    Rating = 1
                },
                new VideoGame()
                {
                    Id = Guid.Parse("0B7AEB9B-EA5D-4F57-92EF-85371F0F0052"),
                    Title = "Bomber Crew",
                    Description = "Chocks away! Bomber Crew is a strategic simulation game, where picking the right crew and keeping your bomber in tip top shape is the difference between completing your mission or losing it all! Each mission is a high-risk expedition where danger comes from every angle. Enemy fighters, flak guns, poor weather, low oxygen and an array of other perilous dangers await when the wheels are up.",
                    ImageURL = "BomberCrew.jpg",
                    CategoryId = context.Categories.FirstOrDefault(q => q.Title == "Strategy").Id,
                    Creator = "Curve Digital",
                    ReleaseDate = new DateTimeOffset(new DateTime(2013,5,26)),
                    Rating = 5
                }
            };

            return videoGames;
        }

        
        private Category[] GenerateCategories()
        {
            var categories = new Category[] {
                new Category() { Id = Guid.Parse("DB731EB4-5194-4BFF-85C6-2490BE805343"), Title = "Kids" , Description = "The perfect place to let your child play safely" },
                new Category() { Id = Guid.Parse("008C6158-CAFF-43E3-8CEF-B6E09118BA74"), Title = "Investigation", Description = "Ideal for playing as a family all round the same screen" },
                new Category() { Id = Guid.Parse("4C2801F3-CF3D-4C21-9F38-40DB13EDD288"), Title = "Sport", Description = "For sharing good times with the family" },
                new Category() { Id = Guid.Parse("0F923A42-5DA3-4FFC-9575-639AD2AC8B54"), Title = "Logic", Description = "A perfect selection for adults who want to relax or disconnect" },
                new Category() { Id = Guid.Parse("8ED3C870-73D4-40F3-BC84-955274CC1848"), Title = "Retro", Description = "For those who are nostalgic for the 80s and 90s" },
                new Category() { Id = Guid.Parse("8DE07853-3562-493D-8B1E-7C91077A9442"), Title = "Adventure", Description = "The invitation to travel"},
                new Category() { Id = Guid.Parse("85C4F82B-5BF8-453A-8929-0BEDDA4C40E8"), Title = "Strategy", Description = "Your tactical experience will be put to test" },
                new Category() { Id = Guid.Parse("D786580A-D4D8-47B1-8AF2-125561565F87"), Title = "Speed", Description = "A selection who games whose common denomiator is speed in general" }
            };

            return categories;
        }

        private Quantity[] GenerateQuantities(VideoGameCatalogDbContext context)
        {
            var quantities = new Quantity[] {
                new Quantity()
                {
                    Id = Guid.Parse("662F4D76-B9B8-43EA-BE6A-4E4D6C56C27B"),
                    VideoGameId = context.VideoGames.FirstOrDefault(q => q.Title == "Angelo Skate Away").Id,
                    IsAvailable = true
                },
                new Quantity()
                {
                    Id = Guid.Parse("72B1D61C-58D3-4EE6-A025-A88E45B6ADE9"),
                    VideoGameId = context.VideoGames.FirstOrDefault(q => q.Title == "Kirikou and the wild beasts").Id,
                    IsAvailable = true
                },
                new Quantity()
                {
                    Id = Guid.Parse("C96065CB-C391-41EF-B83E-04494490CEFB"),
                    VideoGameId = context.VideoGames.FirstOrDefault(q => q.Title == "Momonga Pinball Adventure").Id,
                    IsAvailable = true
                },
                new Quantity()
                {
                    Id = Guid.Parse("E49F85EF-169D-4B22-89AE-7017B322DA72"),
                    VideoGameId = context.VideoGames.FirstOrDefault(q => q.Title == "Stickman Bike: Pro Ride").Id,
                    IsAvailable = true
                },
                new Quantity()
                {
                    Id = Guid.Parse("11EF9D9E-DA54-4EFD-A022-A7E1315297B6"),
                    VideoGameId = context.VideoGames.FirstOrDefault(q => q.Title == "Chroma").Id,
                    IsAvailable = true
                }
            };

            return quantities;
        }
    }
}
