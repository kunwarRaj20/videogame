﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using VideoGameCatalog.Core;

namespace VideoGameCatalog.EntityFramework
{
    public class VideoGameCatalogDbContext : DbContext
    {
        public VideoGameCatalogDbContext() : base("VideoGameCatalog")
        {
            Database.SetInitializer<VideoGameCatalogDbContext>(null);
        }

        public IDbSet<VideoGame> VideoGames { get; set; }

        public IDbSet<Category> Categories { get; set; }

        public IDbSet<Quantity> Quantities { get; set; }

        public virtual void Commit()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

    }
}
